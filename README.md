# 共享单车后台管理系统开发 （未开发完）

### 软件架构
React + react-router + react-redux + Antd + Es6

### 安装使用
1. 下载
``` bash
$ git clone git@gitee.com:yhf7/Protoss.git
```

2. 装包
``` bash
$ sudo cnpm i 或 yarn
```

3. 启动项目
``` bash
$ sudo npm run start 或者 yarn start(荐)
```