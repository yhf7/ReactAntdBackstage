import React, { Component } from 'react'
import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

// 入口跟组件
import App from './App'
import Admin from './admin'

import Common from './common'

// 首页
import Home from './pages/home';
// ui类
import Buttons from './pages/ui/buttons'
import Modals from './pages/ui/modals'
import Loadings from './pages/ui/loadings'
import Notification from './pages/ui/notification'
import Messages from './pages/ui/messages'
import Tabs from './pages/ui/tabs'
import Gallery from './pages/ui/gallery'
import Carousel from './pages/ui/carousel'

// from
import FormLogin from './pages/form/Login'
import FormRegister from './pages/form/register'

// table
import BasicTable from './pages/table/basicTable'
import HighTable from './pages/table/highTable'

// 城市管理
import City from './pages/city'

// 订单管理
import Order from './pages/Order'
// 单车地图
import OrderDetail from './pages/Order/detail'

// 员工管理
import User from './pages/user'

// 车辆管理
import Map from './pages/map'

// 富文本
import Rich from './pages/rich'

// 图表
// 柱状图
import Bar from './pages/charts/bar'
// 饼图
import Pie from './pages/charts/pie'
// 折线图
import Line from './pages/charts/line'

// 权限控制
import Permission from './pages/permission'

// 404
// import NoMatch from './pages/nomatch'

import Login from './pages/login'


export default class IRouter extends Component {
  render() {
    return (
      <Router>
        <App>
          <Switch>
            <Route path="/login" component={ Login } />
            {/* 不添加大括号自动添加return */ }
            <Route path="/common" render={ () =>
              <Common>
                <Route path="/common/order/detail/:orderId" component={ OrderDetail } />
              </Common>
            }
            />
            <Route path="/" render={ () =>
              <Switch>
                <Route path="/admin" render={ () =>
                  <Admin>
                    <Switch>
                      <Route path="/admin/home" component={ Home } />
                      <Route path="/admin/ui/buttons" component={ Buttons } />
                      <Route path="/admin/ui/modals" component={ Modals } />
                      <Route path="/admin/ui/loadings" component={ Loadings } />
                      <Route path="/admin/ui/notification" component={ Notification } />
                      <Route path="/admin/ui/messages" component={ Messages } />
                      <Route path="/admin/ui/tabs" component={ Tabs } />
                      <Route path="/admin/ui/gallery" component={ Gallery } />
                      <Route path="/admin/ui/carousel" component={ Carousel } />
                      <Route path="/admin/form/login" component={ FormLogin } />
                      <Route path="/admin/form/reg" component={ FormRegister } />
                      <Route path="/admin/table/basic" component={ BasicTable } />
                      <Route path="/admin/table/high" component={ HighTable } />
                      <Route path="/admin/city" component={ City } />
                      <Route path="/admin/order" component={ Order } />
                      <Route path="/admin/user" component={ User } />
                      <Route path="/admin/bikeMap" component={ Map } />
                      <Route path="/admin/rich" component={ Rich } />
                      <Route path="/admin/charts/bar" component={ Bar } />
                      <Route path="/admin/charts/pie" component={ Pie } />
                      <Route path="/admin/charts/line" component={ Line } />
                      <Route path="/admin/permission" component={ Permission } />
                      {/* <Route component={ NoMatch } /> */ }
                      <Redirect to="/admin/home" />
                    </Switch>
                  </Admin>
                } />
                {/* 跟路径默认找不到返回到admin */ }
                <Redirect to="/admin" />
              </Switch>
            } />
          </Switch>
        </App>
      </Router>
    )
  }
}