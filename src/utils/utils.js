import React from 'react'
import { Select } from 'antd';
const Option = Select.Option;

export default {
  /**
   * 时间处理
   * @param {new Date().ggetTime} time 
   */
  formateDate (time) {
    if (!time) return '';
    let date = new Date(time);
    return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
  },

  /**
   * 分页公共配置
   * @param {*} data api请求返回的数据
   * @param {*} callback 回调
   */
  pagination(data,callback) {
    return {
      onChange: (current) => {
        callback(current)
      },
      current: data.result.page,
      pageSize: data.result.page_size,
      total: data.result.total_count,
      showTotal: () => {
        return `共${data.result.total_count}条`
      },
      showQuickJumper: true
    }
  },
  /**
   * option子项封装，遍历子项返回
   * @param {Array} optionlist的数据（id以及name）
   */
  getOptionList(data) {
    if (!data) {
      return [];
    }
    // let options = [<Option value="0" key="all_key">全部</Option>]
    let options = []
    data.map(item => {
      return options.push(<Option value={item.id} key={item.id}>{item.name}</Option>)
    })
    return options;
  },
  /**
   * ETable 行点击通用函数
   * @param {*选中行的索引} selectedRowKeys
   * @param {*选中行对象} selectedItem
   */
  updateSelectedItem(selectedRowKeys, selectedRows, selectedIds) {
    if (selectedIds) {
      this.setState({
        selectedRowKeys,
        selectedIds: selectedIds,
        selectedItem: selectedRows
      })
    } else {
      this.setState({
        selectedRowKeys,
        selectedItem: selectedRows
      })
    }
  },
}