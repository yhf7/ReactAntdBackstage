/**
 * 公共表单组件
 * 设置有select选择，input输入框，时间选择，查询以及重置按钮
 * 前面组件最后两个按钮
 * 使用方法，调用组件传入需要的数据值
 */

import React, { Component } from 'react';
import { Input, Select, Form, Checkbox, Button, DatePicker } from 'antd';
import Utils from '../../utils/utils' // 公共方法

const FormItem = Form.Item;

class FilterForm extends Component {

  /**
   * 点击查询
   * 获取表单值
   * 调用父级的点击方法并且把数据传过去
   */
  handleFilterSubmit = () => {
    // 获取表单的值
    let fieldsValue = this.props.form.getFieldsValue()
    this.props.filterSubmit(fieldsValue)
  }

  /**
   * 点击重置，清除表单值恢复默认
   * 调用表单的重置方法
   */
  reset = () => {
    this.props.form.resetFields()
  }

  initFormList = () => {
    // 获取双向表单数据
    const { getFieldDecorator } = this.props.form;
    // 接受父组件传过来的数组数据
    const formList = this.props.formList;
    // 承载jsx的数组
    const formItemList = []
    // 判断是否有数据
    if (formList && formList.length > 0) {
      // 循环遍历构建组件
      formList.forEach(item => {
        // 获取定义数据
        let label = item.label;
        let field = item.field;
        let initialValue = item.initialValue || '';
        let placeholder = item.placeholder;
        let width = item.width;

        // 城市控件
        if (item.type == '城市') {
          const city = <FormItem label="城市" key='city'>
            {
              getFieldDecorator('city',{
                initialValue: "0"
              })(
                <Select
                  style={ { width: 80 } }
                  placeholder={ placeholder }
                >
                  { Utils.getOptionList([{ id: '0', name: '全部' }, { id: '1', name: '北京' }, { id: '2', name: '上海' }, { id: '3', name: '东莞' },]) }
                </Select>
              )
            }
          </FormItem>
          formItemList.push(city)
        }

        // 时间控件
        if (item.type == '时间查询') {
          const begin_time = <FormItem label="订单时间" key='begin_time'>
            {
              getFieldDecorator('begin_time')(
                <DatePicker showTime placeholder={ placeholder } format="YYYY-MM-DD HH:mm:ss" />,
              )
            }
          </FormItem>
          formItemList.push(begin_time)
          // colon 清除分号
          const end_time = <FormItem label="~" colon={false} key='end_time'>
            {
              getFieldDecorator('end_time')(
                <DatePicker showTime placeholder={ placeholder } format="YYYY-MM-DD HH:mm:ss" />,
              )
            }
          </FormItem>
          formItemList.push(end_time)
        }
        // input
        if (item.type == 'INPUT') {
          const INPUT = <FormItem label={ label } key={ field }>
            {
              getFieldDecorator(`${field}`, {
                initialValue: initialValue
              })(
                <Input style={ { width: width} } type="text" placeholder={ placeholder } />
              )
            }
          </FormItem>
          formItemList.push(INPUT)
          // select
        } else if (item.type == 'SELECT') {
          const SELECT = <FormItem label={ label } key={ field }>
            {
              // 切记这里的id只可以用字符串
              getFieldDecorator(`${item.field}`, {
                initialValue: initialValue
              })(
                <Select
                  style={ { width: width } }
                  placeholder={ placeholder }
                >
                  { Utils.getOptionList(item.list) }
                </Select>
              )
            }
          </FormItem>
          formItemList.push(SELECT)
          // checked多选
        } else if (item.type == 'CHECKBOX') {
          const CHECKBOX = <FormItem label={ label } key={ field }>
            {
              getFieldDecorator([field], {
                valuePropName: 'checked',
                initialValue: initialValue // true|false
              })(
                <Checkbox>
                  {label}
                </Checkbox>
              )
            }
          </FormItem>
          formItemList.push(CHECKBOX)
        } else if (item.type == 'DATE') {
          const DatePickers = <FormItem label={label} key={field}>
            {
              getFieldDecorator([field])(
                <DatePicker showTime placeholder={ placeholder } format="YYYY-MM-DD HH:mm:ss" />,
              )
            }
          </FormItem>
          formItemList.push(DatePickers)
        }
      })
    }
    return formItemList
  }

  render() {
    return (
      <div>
        <Form layout="inline">
          {/* 调用方法初始化控件 */}
          { this.initFormList() }
          <FormItem>
            <Button type="primary" style={ { margin: '0 20px' } } onClick={ this.handleFilterSubmit }>查询</Button>
            <Button onClick={ this.reset }>重置</Button>
          </FormItem>
        </Form>
      </div>
    )
  }
}

export default Form.create({})(FilterForm)