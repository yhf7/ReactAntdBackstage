import React, { Component } from 'react'
import { Row, Col } from 'antd'; // antd组件
import Util from '../../utils/utils' // 公共封装函数
import axios from '../../axios' // api请求封装
// 引入redux
import { connect } from 'react-redux'
import './index.less'

class Header extends Component {

  // 组件已进入就执行调函数
  componentWillMount () {
    // 向state设置username
    this.setState({
      userName: "YHF"
    })
    // 开启一个定时器，用来实现动态时间
    setInterval(() => {
      // 请求封装在util的时间处理函数
      const sysTime = Util.formateDate(new Date().getTime())
      // 并设置新的时间
      this.setState({
        sysTime
      })
    }, 1000);
    // 调用请求天气的函数
    this.getWeatherAPIData()
  }

  // 封装的请求天气api的函数
  getWeatherAPIData () {
    // 定义城市
    let city = 'dongguan'
    // 调用axios封装的函数
    axios.jsonp({
      url: 'http://api.map.baidu.com/telematics/v3/weather?location=' + encodeURIComponent(city)+'&output=json&ak=3p49MVra6urFRGOT9s8UBWr2'
      // url: 'https://www.tianqiapi.com/api/?version=v1&city=东莞'
    }).then(res => {
      // 判断数据是否请求成功
      if (res.status === 'success') {
        let data = res.results[0].weather_data[1]// 拿取数据并设置
        this.setState({
          dayPictureUrl: data.dayPictureUrl,
          weather: data.weather
        })
      }
    })
  }

  render() {
    const menuType = this.props.menuType;
    return (
      <div className="header">
        {/* 头部 */}
        <Row className="header-top">
          {
            menuType ? <Col span={ 6 } className="logo">
              <img src="assets/logo-ant.svg" alt=""/>
              <span>IMooc 通用管理系统</span>
            </Col>:""
          }
          {/* 采用珊格 */}
          <Col span={ menuType ? 18 : 24 }>
              <span>欢迎，{this.state.userName}</span>
              <a href='http://localhost:3000/'>退出</a>
          </Col>
        </Row>
        {/* 内容面包屑，判断如果有menuType就不显示没有就显示 */}
        {
          menuType ? '' : <Row className="breadcrumb">
            <Col span={ 3 } className="breadcrumb-title">
              {this.props.menuName}
          </Col>
            {/* 这个就内容 */ }
            <Col span={ 21 } className="weather">
              {/* 时间 */ }
              <span className="date">
                { this.state.sysTime }
              </span>
              {/* 这两个上天气的图片和天气 */ }
              <span className="weather-img">
                <img src={ this.state.dayPictureUrl } alt="" />
              </span>
              <span className="weather-detail">
                { this.state.weather }
              </span>
            </Col>
          </Row>
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    menuName: state.menuName
  }
}

export default connect(mapStateToProps)(Header);