import Jsonp from 'jsonp'
import axios from 'axios'
import { message } from 'antd'
// import {Modal} from 'antd'
import Utils from '../utils/utils'

export default class Axios {

  /**
   * 公共请求列表封装
   * @param {Object} _this 把调用的this传进来
   * @param {Url} url 请求api
   * @param {Object} params 把传给后端的数据传过来
   * @param {Boolean} isMock 判断是否使用mock数据
   */
  static requestList(_this,url, params,isMock) {
    // 传给后端的数据
    let data = {
      params
    }
    // 调用ajax
    this.ajax({
      url,
      data,
      isMock
    }).then(res => {
      
      // 判断是否取到数据
      if (res && res.result) {
        // 循环添加key，表格需要给每个元素配置key
        let list = res.result.item_list.map((item, index) => {
          item.key = index;
          return item;
        });
        
        
        // 设置表格数据以及表格的分页配置，调用Utils中封装的公共配置
        _this.setState({
          list,
          pagination: Utils.pagination(res, (current) => {
            _this.params.page = current;
            _this.requestList();
          })
        })
      }
    })
  }

  /**
   * jsonp请求封装
   * @param {*} options 传入对象，url等
   */
  static jsonp(options) {
    return new Promise((res, rej) => {
      // jsonp解决跨域请求，传入url，和携带回调，通过promise返回
      Jsonp(options.url, {
        param: 'callback'
      }, (err, response) => {
        res(response)
      })
    })
  }

  /**
   * 封装等axios请求函数
   * @param {Object} options 请求的url以及参数等
   * url 路由
   * data {Object} 传给后端的数据
   * isMock {Boolean} 是否需要mock，不传就用服务器的路由(true为mock)
   * isShowLoading {Boolean} 是否需要loading 默认加载（false为关闭）
   */
  static ajax(options) {
    // 定义loading
    let loading;
    // api请求地址
    let baseApi;
    // 判断是否携带参数data，以及判断是否要关闭loading，如果没有进入设置
    if (options.data && options.data.isShowLoading !== false) {
      // 获取dom中id为ajaxLoading的元素并设置style，这个以及放在了index文件里面了
      loading = document.getElementById('ajaxLoading');
      loading.style.display = 'block'
    }
    if (options.isMock) {
      // 请求的默认地址
      baseApi = 'https://www.easy-mock.com/mock/5d4ce55522cb891ea2b4e5a5/mockapi'
    } else {
      baseApi = ''
    }
    // let baseApi = 'https://www.easy-mock.com/mock/5a7278e28d0c633b9c4adbd7/api';
    // 返回一个promise
    return new Promise((res, rej) => {

      // 请求axios传入url等以及回调
      axios({
        url: options.url,
        method: 'get',
        baseURL: baseApi,
        timeout: 5000,
        params: (options.data && options.data.params) || ''
      }).then(response => {
        // 请求完判断关闭loading
        if (options.data && options.data.isShowLoading !== false) {
          loading = document.getElementById('ajaxLoading');
          loading.style.display = 'none'
        }
        // 判断请求成功返回res，非200为错误rej返回，这都是promise里面调元素了
        if (response.status === 200) {
          let resp = response.data
          // if (resp.code === "0") {
          res(resp)
          // } else {
          //   Modal.info({
          //     title: '提示',
          //     content: resp
          //   })
          // }
        } else {
          rej(response)
        }
      }).catch(res => {
        // 请求完判断关闭loading
        if (options.data && options.data.isShowLoading !== false) {
          loading = document.getElementById('ajaxLoading');
          loading.style.display = 'none'
        }
        message.error('数据加载失败');
      })
    })
  }
}