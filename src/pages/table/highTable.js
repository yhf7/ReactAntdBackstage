/**
 * 高级表格
 * 1. 固定表格头部内容滚动
 * 2. 左右侧固定中间滚动
 * 3. 表格内部列排序
 * 4. 添加操作按钮
 */

import React, { Component } from 'react'
import axios from '../../axios'
import { Card, Table, Badge, Modal, message, Button } from 'antd'

export default class BasicTable extends Component {

  // 初始化
  state = {}

  // 自定义请求初始化数据
  params = {
    page: 1 // 分页树
  }

  // dom执行完跑
  componentDidMount() {
    // 请求列表数据
    this.request()
  }

  // 动态获取mock数据
  request = () => {
    // let _this = this;
    axios.ajax({
      url: '/table/high/list',
      data: {
        params: {
          page: this.params.page
        }
      },
      isMock: true
    }).then(res => {
      if (res.code ===0) {
         // 循环加key
        res.result.list.map((item,i) => {
          return item.key = i
        })
        // 设置列表数据
        this.setState({
          dataSource: res.result.list,
        })
      }

    })
  }

  /**
   * 表格内部排序点击事件
   */
  onChange = (pagination, filters, sorter) => {
    console.log('params', pagination, filters, sorter);
  }

  // 删除对象
  handleDelete = item => {
    Modal.confirm({
      title: '提示',
      content: `您确定删除这些数据吗?`,
      onOk: () => {
        message.success('删除成功')
      }
    })
  }

  

  render() {
    // 定制表头
    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
        width: 150,
      },
      {
        title: '用户名',
        dataIndex: 'username',
        width: 150,
      },
      {
        title: '性别',
        dataIndex: 'sex',
        width: 150,
        render(sex) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '状态',
        dataIndex: 'state',
        width: 150,
        render(state) {
          let config = {
            '1': '咸鱼一条',
            '2': '风华浪子',
            '3': '北大才子',
            '4': '百度FE',
            '5': '创业者'
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        width: 150,
        render(abc) {
          let config = {
            '1': '游泳',
            '2': '打篮球',
            '3': '踢足球',
            '4': '跑步',
            '5': '爬山',
            '6': '骑行',
            '7': '桌球',
            '8': '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '地址',
        dataIndex: 'address',
        width: 150,
      },
      {
        title: '早起时间',
        dataIndex: 'time',
        width: 150,
      }
    ];

    // 定制表头2
    const columns2 = [
      {
        title: 'id',
        dataIndex: 'id',
        width: 150,
        fixed: 'left',
      },
      {
        title: '用户名',
        dataIndex: 'username',
        width: 150,
        fixed: 'left',
      },
      {
        title: '性别',
        dataIndex: 'sex',
        width: 150,
        render(sex) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '状态',
        dataIndex: 'state',
        width: 150,
        render(state) {
          let config = {
            '1': '咸鱼一条',
            '2': '风华浪子',
            '3': '北大才子',
            '4': '百度FE',
            '5': '创业者'
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        width: 150,
        render(abc) {
          let config = {
            '1': '游泳',
            '2': '打篮球',
            '3': '踢足球',
            '4': '跑步',
            '5': '爬山',
            '6': '骑行',
            '7': '桌球',
            '8': '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '地址',
        dataIndex: 'address',
        width: 150,
        fixed: 'right',
      },
      {
        title: '早起时间',
        dataIndex: 'time',
        width: 150,
        fixed: 'right',
      }
    ];
    // 定制表头3
    const columns3 = [
      {
        title: 'id',
        dataIndex: 'id',
      },
      {
        title: '用户名',
        dataIndex: 'username',
      },
      {
        title: '性别',
        dataIndex: 'sex',
        render(sex) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '年龄',
        dataIndex: 'age',
        width: 150,
        sorter: (a, b) => a.age - b.age,
      },
      {
        title: '状态',
        dataIndex: 'state',
        render(state) {
          let config = {
            '1': '咸鱼一条',
            '2': '风华浪子',
            '3': '北大才子',
            '4': '百度FE',
            '5': '创业者'
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        render(abc) {
          let config = {
            '1': '游泳',
            '2': '打篮球',
            '3': '踢足球',
            '4': '跑步',
            '5': '爬山',
            '6': '骑行',
            '7': '桌球',
            '8': '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
      },
      {
        title: '地址',
        dataIndex: 'address',
      },
      {
        title: '早起时间',
        dataIndex: 'time',
      }
    ];
    // 定制表头4
    const columns4 = [
      {
        title: 'id',
        dataIndex: 'id',
      },
      {
        title: '用户名',
        dataIndex: 'username',
      },
      {
        title: '性别',
        dataIndex: 'sex',
        render(sex) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '年龄',
        dataIndex: 'age',
        width: 150
      },
      {
        title: '状态',
        dataIndex: 'state',
        render(state) {
          let config = {
            '1': <Badge status="success" text='咸鱼一条'/>,
            '2': <Badge status="error" text='风华浪子'/>,
            '3': <Badge status="default" text='北大才子'/>,
            '4': <Badge status="processing" text='百度FE'/>,
            '5': <Badge status="warning" text='创业者'/>
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        render(abc) {
          let config = {
            '1': '游泳',
            '2': '打篮球',
            '3': '踢足球',
            '4': '跑步',
            '5': '爬山',
            '6': '骑行',
            '7': '桌球',
            '8': '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
      },
      {
        title: '地址',
        dataIndex: 'address',
      },
      {
        title: '操作',
        render: (text,item) => {
          return <Button onClick={ (item) => { this.handleDelete(item)}}>删除</Button>
        }
      }
    ];
    
    return (
      <div>
        <Card title="头部固定">
          <Table columns={ columns } dataSource={ this.state.dataSource } bordered pagination={ false } scroll={ { y: 240 } } />
        </Card>
        <Card title="左侧固定" style={ { margin: '10px 0' } }>
          <Table columns={ columns2 } dataSource={ this.state.dataSource } bordered pagination={ false } scroll={ {  x: 2500, y: 300 } } />
        </Card>
        <Card title="表格排序" style={ { margin: '10px 0' } }>
          <Table columns={ columns3 } dataSource={ this.state.dataSource } bordered pagination={ false } onChange={this.onChange} />
        </Card>
        <Card title="操作按钮" style={ { margin: '10px 0' } }>
          <Table columns={ columns4 } dataSource={ this.state.dataSource } bordered pagination={ false } />
        </Card>
      </div>
    )
  }
}