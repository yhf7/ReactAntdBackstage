/**
 * 基础表格页面
 * 基础表格，动态渲染数据，添加单选，添加多选，添加分页
 */

import React, { Component } from 'react'
import axios from '../../axios'
import { Card, Table, Modal, message, Button } from 'antd'
import Utils from '../../utils/utils'

export default class BasicTable extends Component {

  state = {}

  params = {
    page:1
  }

  componentDidMount() {
    const dataSource = [
      {
        key: '1',
        id: '0',
        username: 'YHF',
        sex: '20',
        state: '1',
        interest: '1',
        birthday: '2000-01-01',
        address: '广东东莞厚街',
        time: '09:00'
      },
      {
        key: '2',
        id: '1',
        username: 'YHF2',
        sex: '20',
        state: '1',
        interest: '1',
        birthday: '2000-01-01',
        address: '广东东莞厚街',
        time: '09:00'
      },
      {
        key: '3',
        id: '2',
        username: 'YHF3',
        sex: '20',
        state: '1',
        interest: '1',
        birthday: '2000-01-01',
        address: '广东东莞厚街',
        time: '09:00'
      },
    ]
    this.setState({
      dataSource
    })
    this.request()
  }

  // 动态获取mock数据
  request = () => {
    let _this = this;
    axios.ajax({
      url: '/table/list',
      data: {
        params: {
          page: this.params.page
        }
      },
      isMock: true
    }).then(res => {

      this.setState({
        dataSource2: res.result.list,
        selectedRowKeys: [],
        selectedRows: null,
        pagination: Utils.pagination(res,(current) => {
          _this.params.page = current;
          _this.request()
        })
      })
    })
  }

  onRowClick = (rec, i) => {
    let selectKey = [i + 1]
    this.setState({
      selectedRowKeys: selectKey,
      selectedItem: rec
    })
  }

  // 多选删除操作
  handleDelete = () => {
    let rows = this.state.selectedRows;
    let ids = []
    if (!rows) {
      return false
    }
    rows.map((item) => {
      return ids.push(item.id)
    })

    Modal.confirm({
      title: '提示',
      content: `您确定删除这些数据吗?${ids}`,
      onOk: () => {
        message.success('删除成功')
        this.request();
      }
    })
  }

  render() {
    // 定制表头
    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
      },
      {
        title: '用户名',
        dataIndex: 'username'
      },
      {
        title: '性别',
        dataIndex: 'sex',
        render(sex) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '状态',
        dataIndex: 'state',
        render(state) {
          let config = {
            '1': '咸鱼一条',
            '2': '风华浪子',
            '3': '北大才子',
            '4': '百度FE',
            '5': '创业者'
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        render(abc) {
          let config = {
            '1': '游泳',
            '2': '打篮球',
            '3': '踢足球',
            '4': '跑步',
            '5': '爬山',
            '6': '骑行',
            '7': '桌球',
            '8': '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday'
      },
      {
        title: '地址',
        dataIndex: 'address'
      },
      {
        title: '早起时间',
        dataIndex: 'time'
      }
    ];

    const { selectedRowKeys } = this.state;

    const rowSelection = {
      type: 'radio',
      selectedRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRowKeys: selectedRowKeys,
          selectedItem: selectedRows
        })
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      },
      getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };

    const rowCheckSelection = {
      // type: 'checkbox',
      selectedRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRowKeys,
          selectedRows
        })
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      },
      getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };

    return (
      <div>
        <Card title="基础表格">
          <Table columns={ columns } dataSource={ this.state.dataSource } bordered pagination={ false } />
        </Card>
        <Card title="动态数据渲染" style={ { margin: '10px 0' } }>
          <Table columns={ columns } dataSource={ this.state.dataSource2 } bordered pagination={ false } />
        </Card>
        <Card title="Mock-单选" style={ { margin: '10px 0' } }>
          <Table onRow={ (record, index) => {
            return {
              onClick: () => {
                this.onRowClick(record, index)
              },
            };
          } } rowSelection={ rowSelection } columns={ columns } dataSource={ this.state.dataSource2 } bordered pagination={ false } />
        </Card>
        <Card title="Mock-多选" style={ { margin: '10px 0' } }>
          <div style={{marginBottom: 10}}>
            <Button onClick={ this.handleDelete }>删除</Button>
          </div>
          <Table onRow={ (record, index) => {
            return {
              onClick: () => {
                this.onCheckClick(record, index)
              },
            };
          } } rowSelection={ rowCheckSelection } columns={ columns } dataSource={ this.state.dataSource2 } bordered pagination={ false } />
        </Card>
        <Card title="Mock-表格分页" style={ { margin: '10px 0' } }>
          <Table columns={ columns } dataSource={ this.state.dataSource2 } bordered pagination={ this.state.pagination } />
        </Card>
      </div>
    )
  }
}