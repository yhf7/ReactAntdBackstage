import React,{Component} from 'react';
import axios from './../../axios'
import BaseForm from '../../components/BaseForm'
import {Card} from 'antd'

export default class Map extends Component {

  state = {}

  // 自定义params数据，请求时默认数据
  params = {
    
  }
  map = ''

  // 头部表单组件
  formList = [
    {
      type: '城市'
    },
    {
      type: '时间查询'
    },
    {
      type: 'SELECT',
      label: '订单状态',
      field: 'order_status',
      placeholder: '全部',
      initialValue: '0',
      width: 100,
      list: [{ 'id': "0", name: '全部' }, { 'id': "1", name: '进行中' }, { 'id': "2", name: '行程结束' }]
    },
  ]

  // 执行完dom操作后运行
  componentDidMount() {
    // 默认调用请求数据
    this.requestList();
  }

  // 默认请求我们的接口数据
  requestList = () => {
    axios.ajax({
      url: '/map/bike_list',
      data: {
        params: this.params
      },
      isMock: true
    }).then(res => {
      if (res.code == 0) {
        let data = res.result;
        this.setState({
          total_count: data.total_count
        })
        this.renderMap(data)
      }
    })
  }

  /**
   * 渲染地图数据
   */
  renderMap = res => {
    // 初始化地图
    this.map = new window.BMap.Map("containerMap", { enableMapClick: false });
    // 添加地图控件
    this.addMapControl()
    // 绘制用户路线图
    this.drawBikeRoute(res.route_list)
    // 绘制服务区
    this.drwaServiceArea(res.service_list)
    // 绘制自行车图标
    this.drwasBikePoint(res.bike_list)
  }

  /**
  * 添加地图控件
  */
  addMapControl = () => {
    let map = this.map;
    map.addControl(new window.BMap.ScaleControl({ anchor: window.BMAP_ANCHOR_TOP_RIGHT }));// 比例尺,控件定位于地图的右上角
    map.addControl(new window.BMap.NavigationControl({ anchor: window.BMAP_ANCHOR_TOP_RIGHT }));// 平移缩放控件
  }

  /**
   * 绘制用户起始坐标
   * 绘制用户路线
   * @param {Array} list 用户路线的列表数据 
   */
  drawBikeRoute = list => {
    // 以逗号分割数据
    let gps1 = list[0].split(',');
    // 初始化起点坐标 point 就是坐标位置的创建
    let startPoint = new window.BMap.Point(gps1[0], gps1[1])
    let gps2 = list[list.length - 1].split(',');
    let endPoint = new window.BMap.Point(gps2[0], gps2[1])

    // 定义中心位置
    this.map.centerAndZoom(endPoint, 11);
    // 起点的icon创建
    let startPointIcon = new window.BMap.Icon('/assets/start_point.png', new window.BMap.Size(36, 42), {
      imageSize: new window.BMap.Size(36, 42),
      anchor: new window.BMap.Size(18, 42) // 坐标图片偏移
    })
    // 初始化坐标，第一位坐标点 第二位图标 （marker坐标点）
    let bikeMarkerStart = new window.BMap.Marker(startPoint, { icon: startPointIcon });
    // 添加到地图
    this.map.addOverlay(bikeMarkerStart);
    let endPointIcon = new window.BMap.Icon('/assets/end_point.png', new window.BMap.Size(36, 42), {
      imageSize: new window.BMap.Size(36, 42),
      anchor: new window.BMap.Size(18, 42)
    })
    // 初始化坐标，第一位坐标点 第二位图标 （marker坐标点）
    let bikeMarkerEnd = new window.BMap.Marker(endPoint, { icon: endPointIcon });
    // 添加到地图
    this.map.addOverlay(bikeMarkerEnd);

    // 连接路线图
    let trackPoint = [];
    // 记录坐标点
    for (let i = 0; i < list.length; i++) {
      let point = list[i].split(',');
      // 添加坐标点到数组
      trackPoint.push(new window.BMap.Point(point[0], point[1]));
    }

    // Polyline 画线，第一位输入坐标点数组，第二位是个对象可以修改线颜色宽度等
    let polyline = new window.BMap.Polyline(trackPoint, {
      strokeColor: '#1869AD',
      strokeWeight: 3,
      strokeOpacity: 1
    })
    // 添加到地图
    this.map.addOverlay(polyline);
  }

  /**
   * 绘制服务区
   * @param {Array} positionList 服务器圈经纬度数组
   */
  drwaServiceArea = positionList => {
    // 连接路线图
    let trackPoint = [];
    // 记录坐标点
    for (let i = 0; i < positionList.length; i++) {
      let point = positionList[i];
      // 添加坐标点到数组
      trackPoint.push(new window.BMap.Point(point.lon, point.lat));
    }
    // 绘制服务区
    let polygon = new window.BMap.Polygon(trackPoint, {
      strokeColor: '#CE0000', // 线颜色
      strokeWeight: 3, // 线宽度
      strokeOpacity: 1, // 透明度
      // fillColor: '#ff8605', // 填充颜色
      fillOpacity: 0.4 // 填充透明度
    })
    this.map.addOverlay(polygon);
  }

  /**
   * 单车坐标点，可用单车
   * @param {Array} bikeList 可用但是的经纬度列表数据
   */
  drwasBikePoint = bikeList => {
    let bikeIcon = new window.BMap.Icon('/assets/bike.jpg', new window.BMap.Size(36, 42), {
      imageSize: new window.BMap.Size(36, 42),
      anchor: new window.BMap.Size(18, 42)
    })
    bikeList.forEach(item => {
      let p = item.split(',');
      let point = new window.BMap.Point(p[0], p[1])
      // 初始化坐标，第一位坐标点 第二位图标 （marker坐标点）
      let pointMarker = new window.BMap.Marker(point, { icon: bikeIcon });
      // 添加到地图
      this.map.addOverlay(pointMarker);
    })
  }

  // 子组件调用方法
  /**
   * 子组件点击查询
   */
  handleFilter = (params) => {
    this.params = params
    this.requestList()
  }

  render () {
    return (
      <div>
        <Card>
          {/* 调用组件 */ }
          <BaseForm filterSubmit={ this.handleFilter } formList={ this.formList } />
        </Card>
        <Card style={{marginTop: 20}}>
          <div>共{ this.state.total_count}辆</div>
          <div id="containerMap" style={{height:600}}></div>
        </Card>
      </div>
    )
  }
}