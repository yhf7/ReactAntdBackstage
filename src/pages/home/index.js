/**
 * 主页文件
 * 展示欢迎界面
 */
import React, { Component } from 'react';
import './index.less'

export default class Home extends Component {
  render () {
    return (
      <div className="home-wrap">
        欢迎使用IMooc后台管理系统
      </div>
    )
  }
}
