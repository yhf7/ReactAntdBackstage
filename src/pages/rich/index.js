import React, { Component } from 'react'
import { Card,Button,Modal } from 'antd'
import { Editor } from 'react-draft-wysiwyg';
import draftjs from 'draftjs-to-html'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

export default class Rich extends Component {
  state = {
    showRichText: false, // 是否显示状态弹框
    editorContent: '', // 文本数据
    editorState: '', // 编辑器状态数据
  };

  /**
   * 清除内容
   */
  handleClearContent = () => {
    this.setState({
      editorState: ''
    })
  }

  /**
   * 获取富文本的内容
   */
  handleGetText = () => {
    this.setState({
      showRichText: true
    })
  }


  /**
   * 编辑器内容变更
   * @param {String} editorContent 数据
   */
  onEditorChange = (editorContent) => {
    this.setState({
      editorContent,
    });
  };

  /**
   * 编辑器状态变更
   */
  onEditorStateChange = (editorState) => {
    this.setState({
      editorState
    });
  };

  render() {
    const { editorState } = this.state;
    return (
      <div>
        <Card style={ { marginTop: 10 } }>
          <Button type="primary" onClick={ this.handleClearContent }>清空内容</Button>
          <Button type="primary" onClick={ this.handleGetText }>获取HTML文本</Button>
        </Card>
        <Card title="富文本编辑器">
          <Editor
            editorState={ editorState }
            onContentStateChange={ this.onEditorChange }
            onEditorStateChange={ this.onEditorStateChange }
          />
        </Card>
        <Modal
          title="富文本"
          visible={ this.state.showRichText }
          onCancel={ () => {
            this.setState({
              showRichText: false
            })
          } }
          footer={ null }
        >
          { draftjs(this.state.editorContent) }
        </Modal>
      </div>
    )
  }
}