/**
 * 单车详情组件
 * 百度地图api，地图，单车路线图，单车使用区域
 * 用户信息
 */

import React,{Component} from 'react'
import { Card } from 'antd'
import axios from './../../axios/';

import './detail.less'

export default class Details extends Component {
  state = {}

  // dom执行完毕执行
  componentDidMount () {
    // 获取路由带过来的参数
    let orderId = this.props.match.params.orderId;
    // 判断有获取到id就调用请求获取数据
    if (orderId) {
      this.getDetailInfo(orderId)
    }
  }

  /**
   * 获取页面的数据
   * @param {Number} orderId 数据的id编号，通过编号进行获取数据
   */
  getDetailInfo = orderId => {
    axios.ajax({
      url: '/order/detail',
      data: {
        params: {
          orderId
        }
      },
      isMock: true
    }).then(res => {
      if (res.code == 0) {
        this.setState({
          orderInfo: res.result
        })
        // 调用构建地图
        this.renderMap(res.result)
      }
    })
  }

  
  /**
   * 构建地图
   * @param {Object} result api返回的数据
   */
  renderMap = result => {
    this.map = new window.BMap.Map("orderDetailMap",{enableMapClick:false});
    // this.map.centerAndZoom('北京', 11);
    // 添加地图控件
    this.addMapControl()
    // 调用绘制用户的行驶路线图
    this.drawBikeRoute(result.position_list)

    this.drwaServiceArea(result.area)
  }

  /**
   * 添加地图控件
   */
  addMapControl = () => {
    let map = this.map;
    map.addControl(new window.BMap.ScaleControl({ anchor: window.BMAP_ANCHOR_TOP_RIGHT }));// 比例尺,控件定位于地图的右上角
    map.addControl(new window.BMap.NavigationControl({ anchor: window.BMAP_ANCHOR_TOP_RIGHT }));// 平移缩放控件
  }

  /**
   * 绘制用户的行驶路线图
   */
  drawBikeRoute = positionList => {
    let startPoint = '';
    let endPoint = '';
    if (positionList.length>0) {
      // 启始坐标点
      let first = positionList[0];
      // 结束坐标点
      let last = positionList[positionList.length - 1];
      // 创建坐标点
      startPoint = new window.BMap.Point(first.lon, first.lat);
      // 创建启动图标，第一位是图标第二位设置size
      let startIcon = new window.BMap.Icon('/assets/start_point.png', new window.BMap.Size(36, 42), {
        imageSize: new window.BMap.Size(36, 42), // 添加图片大小
        anchor: new window.BMap.Size(18, 42) // 放的位置
      })

      // 初始化坐标，第一位坐标点 第二位图标 （marker坐标点）
      let startMarker = new window.BMap.Marker(startPoint, { icon: startIcon });
      // 添加到地图
      this.map.addOverlay(startMarker);

      // 创建坐标点
      endPoint = new window.BMap.Point(last.lon, last.lat);
      // 创建启动图标，第一位是图标第二位设置size
      let endIcon = new window.BMap.Icon('/assets/end_point.png', new window.BMap.Size(36, 42), {
        imageSize: new window.BMap.Size(36, 42),
        anchor: new window.BMap.Size(18, 42)
      })
      // 初始化坐标，第一位坐标点 第二位图标 （marker坐标点）
      let endMarker = new window.BMap.Marker(endPoint, { icon: endIcon });
      // 添加到地图
      this.map.addOverlay(endMarker);

      // 连接路线图
      let trackPoint = [];
      // 记录坐标点
      for (let i = 0; i < positionList.length; i++) {
        let point = positionList[i];
        // 添加坐标点到数组
        trackPoint.push(new window.BMap.Point(point.lon, point.lat));
      }

      // Polyline 画线，第一位输入坐标点数组，第二位是个对象可以修改线颜色宽度等
      let polyline = new window.BMap.Polyline(trackPoint, {
        strokeColor: '#1869AD',
        strokeWeight: 3,
        strokeOpacity: 1
      })
      // 添加到地图
      this.map.addOverlay(polyline);
      // 方法对地图进行初始化，中心点
      this.map.centerAndZoom(endPoint, 11);
    }
  }

  // 绘制服务区
  drwaServiceArea = positionList => {
    // 连接路线图
    let trackPoint = [];
    // 记录坐标点
    for (let i = 0; i < positionList.length; i++) {
      let point = positionList[i];
      // 添加坐标点到数组
      trackPoint.push(new window.BMap.Point(point.lon, point.lat));
    }
    // 绘制服务区
    let polygon = new window.BMap.Polygon(trackPoint, {
      strokeColor: '#CE0000', // 线颜色
      strokeWeight: 4, // 线宽度
      strokeOpacity: 1, // 透明度
      fillColor: '#ff8605', // 填充颜色
      fillOpacity: 0.4 // 填充透明度
    })
    this.map.addOverlay(polygon);
  }

  render () {
    const info = this.state.orderInfo || {}
    return (
      <div>
        <Card>
          <div id="orderDetailMap" className="order-map"></div>
          <div className="detail-items">
            <div className="item-title">基础信息</div>
            <ul className="detail-form">
              <li>
                <div className="detail-form-left">用车模式</div>
                <div className="detail-form-content">{ info.mode == 1 ? '服务区' : '停车点' }</div>
              </li>
              <li>
                <div className="detail-form-left">订单编号</div>
                <div className="detail-form-content">{ info.order_sn }</div>
              </li>
              <li>
                <div className="detail-form-left">车辆编号</div>
                <div className="detail-form-content">{ info.bike_sn }</div>
              </li>
              <li>
                <div className="detail-form-left">用户姓名</div>
                <div className="detail-form-content">{ info.user_name }</div>
              </li>
              <li>
                <div className="detail-form-left">手机号码</div>
                <div className="detail-form-content">{ info.mobile }</div>
              </li>
            </ul>
          </div>
          <div className="detail-items">
            <div className="item-title">行驶轨迹</div>
            <ul className="detail-form">
              <li>
                <div className="detail-form-left">行程起点</div>
                <div className="detail-form-content">{ info.start_location }</div>
              </li>
              <li>
                <div className="detail-form-left">行程终点</div>
                <div className="detail-form-content">{ info.end_location }</div>
              </li>
              <li>
                <div className="detail-form-left">行驶里程</div>
                <div className="detail-form-content">{ info.distance / 1000 }公里</div>
              </li>
            </ul>
          </div>
        </Card>
      </div>
    )
  }
}