import React, { Component } from 'react';
import { Card, Button, Modal } from 'antd';

// 子组件
import UserAuthForm from './assembly/UserAuthForm';
import PermForm from './assembly/PermForm'
import RoleForm from './assembly/RoleForm'

import ETable from './../../components/ETable'
import Utils from './../../utils/utils';
import axios from './../../axios'

export default class PermissionUser extends Component {

  state = {
    isRoleVisible: false, // 控制创建用户显示隐藏
    isPermVisible: false, // 控制设置权限显示隐藏
    isUserVisible: false, // 控制用户授权显示隐藏
    detailInfo: {}, // 选中的数据，传入给子组件
    menuInfo: [], // 权限选中数组
  }

  /**
   * 页面一进入就请求获取API
   */
  componentWillMount() {
    this.requestList()
  }

  /**
   * 请求函数
   */
  requestList = () => {
    // 调用公共封装的列表请求函数
    axios.requestList(this, '/role/list', {}, true)
  }

  /**
   * 点击创建用户
   */
  handleClearContent = () => {
    this.setState({
      isRoleVisible: true
    })
  }

  /**
   * 设置权限
   */
  handlePermission = () => {
    let item = this.state.selectedItem;
    if (!item) {
      Modal.info({
        title: '信息',
        content: '请选择一个角色'
      })
      return;
    }
    this.setState({
      isPermVisible: true,
      detailInfo: item,
      menuInfo: item.menus
    })
  }

  /**
   * 用户授权
   */
  handleUserAuth = () => {
    let item = this.state.selectedItem;
    if (!item) {
      Modal.info({
        title: '信息',
        content: '未选中任何项目'
      })
      return;
    }
    // 调用请求用户数据
    this.getRoleUserList(this.state.selectedItem.id);
    this.setState({
      isUserVisible: true,
      detailInfo: this.state.selectedItem // 赋值选中的数据
    })
  }

  /**
   * 请求权限用户数据数据
   * @param {Number} id 选择类别的id
   */
  getRoleUserList = (id) => {
    axios.ajax({
      url: '/role/user_list',
      data: {
        params: {
          id: id
        }
      },
      isMock: true
    }).then((res) => {
      if (res) {
        // 调用构建穿梭框的数据
        this.getAuthUserList(res.result);
      }
    })
  }

  /**
   * 筛选目标用户
   * @param {array} dataSource 请求回来的用户数据
   */
  getAuthUserList = (dataSource) => {
    const mockData = [];
    const targetKeys = [];
    if (dataSource && dataSource.length > 0) {
      for (let i = 0; i < dataSource.length; i++) {
        const data = {
          key: dataSource[i].user_id,
          title: dataSource[i].user_name,
          status: dataSource[i].status,
        };
        if (data.status == 1) {
          targetKeys.push(data.key);
        }
        mockData.push(data);
      }
    }
    this.setState({ mockData, targetKeys });
  };

  /**
   * 子组件
   */
  /**
  * 创建角色提交
  */
  handleRoleSubmit = () => {
    // 调用验证
    this.roleForm.props.form.validateFields(err => {
      // 通过验证执行
      if (!err) {
        // 获取表单数据
        let data = this.roleForm.props.form.getFieldsValue();
        axios.ajax({
          url: '/role/create',
          data: {
            params: data
          },
          isMock: true
        }).then(res => {
          if (res.code == 0) {
            this.setState({
              isRoleVisible: false // 关闭弹框
            })
            this.roleForm.props.form.resetFields() // 重置表单
            this.requestList()
          }
        })
      }
    });
  }

  /**
   * 权限控制提交
   */
  handlePermSubmit = () => {
    let data = this.permForm.props.form.getFieldsValue();
    // 获取当前选中的数据id
    data.role_id = this.state.selectedItem.id;
    // 获取赋值权限选中数据
    data.menus = this.state.menuInfo;
    axios.ajax({
      url: '/permission/edit',
      data: {
        params: {
          ...data
        }
      },
      isMock: true
    }).then(res => {
      this.setState({
        isPermVisible: false
      })
      this.permForm.props.form.resetFields()
      this.requestList()
    })
  }

  /**
   * 用户授权
   */
  handleUserAuthSubmit = () => {
    let data = {};
    data.user_ids = this.state.targetKeys || [];
    data.role_id = this.state.selectedItem.id;
    axios.ajax({
      url: '/role/user_role_edit',
      data: {
        params: {
          ...data
        }
      },
      isMock: true
    }).then((res) => {
      if (res) {
        this.setState({
          isUserVisible: false
        })
        this.requestList();
      }
    })
  }

  /**
   * 记录子组件穿梭框右侧保留的数据
   */
  patchUserInfo = (targetKeys) => {
    this.setState({
      targetKeys: targetKeys
    });
  };

  render() {

    const columns = [
      {
        title: '角色ID',
        dataIndex: 'id'
      },
      {
        title: '角色名称',
        dataIndex: 'role_name'
      },
      {
        title: '创建时间',
        dataIndex: 'create_time',
        render: Utils.formateDate,
        width: 200
      },
      {
        title: '使用状态',
        dataIndex: 'status',
        render(status) {
          return status == 1 ? '启用' : '禁用'
        }
      },
      {
        title: '授权时间',
        dataIndex: 'authorize_time',
        render: Utils.formateDate,
        width: 200
      },
      {
        title: '授权人',
        dataIndex: 'authorize_user_name'
      },
    ]

    return (
      <div>
        <Card>
          <Button type="primary" onClick={ this.handleClearContent }>创建角色</Button>
          <Button type="primary" onClick={ this.handlePermission }>设置权限</Button>
          <Button type="primary" onClick={ this.handleUserAuth }>用户授权</Button>
        </Card>
        <div className="content-wrap">
          <ETable
            updateSelectedItem={ Utils.updateSelectedItem.bind(this) }
            selectedRowKeys={ this.state.selectedRowKeys }
            dataSource={ this.state.list }
            columns={ columns }
            pagination={ this.state.pagination } />
        </div>
        <Modal
          title="创建角色"
          visible={ this.state.isRoleVisible }
          onOk={ this.handleRoleSubmit }
          onCancel={ () => {
            this.roleForm.props.form.resetFields()
            this.setState({
              isRoleVisible: false
            })
          } }>
          <RoleForm wrappedComponentRef={ (inst) => this.roleForm = inst } />
        </Modal>
        <Modal
          title="控制权限"
          visible={ this.state.isPermVisible }
          width={ 600 }
          onOk={ this.handlePermSubmit }
          onCancel={ () => {
            this.permForm.props.form.resetFields()
            this.setState({
              isPermVisible: false
            })
          } }>
          <PermForm
            detailInfo={ this.state.detailInfo }
            menuInfo={ this.state.menuInfo }
            patchMenuInfo={ (checkedKeys) => {
              this.setState({
                menuInfo: checkedKeys
              })
            } }
            wrappedComponentRef={ (inst) => this.permForm = inst } />
        </Modal>
        <Modal
          title="用户授权"
          width={800}
          visible={ this.state.isUserVisible }
          onOk={ this.handleUserAuthSubmit }
          onCancel={ () => {
            this.userAuthForm.props.form.resetFields()
            this.setState({
              isUserVisible: false
            })
          } }>
          <UserAuthForm
            wrappedComponentRef={ (inst) => this.userAuthForm = inst }
            detailInfo={ this.state.detailInfo }
            targetKeys={ this.state.targetKeys }
            mockData={ this.state.mockData }
            patchUserInfo={ this.patchUserInfo } />
        </Modal>
      </div>
    )
  }
}



