import React,{Component} from 'react';
import {Form,Input,Select} from 'antd';
const FormItem = Form.Item;
const { Option } = Select;

/**
 * 创建角色子组件
 */
class RoleForm extends Component {
  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 }
    }
    return (
      <Form layout="horizontal" { ...formItemLayout }>
        <FormItem label="角色名称">
          {
            getFieldDecorator('role_name', {
              rules: [
                {
                  required: true,
                  message: '请输入角色名称',
                },
              ],
            })(
              <Input type="text" placeholder="请输入角色名称" />
            )
          }
        </FormItem>
        <FormItem label="状态">
          {
            getFieldDecorator('state', {
              initialValue: 1
            })(
              <Select style={ { width: 120 } }>
                <Option value={ 1 }>开启</Option>
                <Option value={ 0 }>关闭</Option>
              </Select>
            )
          }
        </FormItem>
      </Form>
    )
  }
}

export default RoleForm = Form.create({})(RoleForm);