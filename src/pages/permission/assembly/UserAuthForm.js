import React, { Component } from 'react';
import {  Form, Input,Transfer } from 'antd';
const FormItem = Form.Item;
/**
 * 用户授权子函数
 */
class UserAuthForm extends Component {

  /**
   * 数据筛选函数
   */
  filterOption = (inputValue, option) => {
    return option.title.indexOf(inputValue) > -1;
  };

  /**
   * 选择时的回调
   * @param {array} targetKey 返回值，右侧返回数据
   * 调用父级保存数据函数传入数据
   */
  handleChange = (targetKeys) => {
    this.props.patchUserInfo(targetKeys);
  };

  render() {
    const detail_info = this.props.detailInfo;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 18 }
    }
    return (
      <Form layout="horizontal" { ...formItemLayout }>
        <FormItem label="角色名称">
          {

            <Input style={ { width: 120 } } type="text" placeholder={ detail_info.role_name } disabled />

          }
        </FormItem>
        <FormItem label="选择用户">
          <Transfer
            listStyle={ { width: 200, height: 400 } }
            dataSource={ this.props.mockData }
            showSearch
            titles={ ['待选用户', '已选用户'] }
            filterOption={ this.filterOption }
            targetKeys={ this.props.targetKeys }
            onChange={ this.handleChange }
            onSearch={ this.handleSearch }
            render={ item => item.title }
          />
        </FormItem>
      </Form>
    )
  }
}

export default UserAuthForm = Form.create({})(UserAuthForm);