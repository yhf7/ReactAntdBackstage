import React, { Component } from 'react';
import { Form, Input, Select, Tree } from 'antd';
// 权限数据
import menuConfig from '../../../config/menuConfig';
const FormItem = Form.Item;
const { Option } = Select;
const { TreeNode } = Tree

/**
 * 权限控制子组件
 * 表单
 */
class PermForm extends Component {

  renderThreeNodes = (data) => {
    return data.map(item => {
      if (item.children) {
        return <TreeNode { ...item }>
          { this.renderThreeNodes(item.children) }
        </TreeNode>
      } else {
        return <TreeNode { ...item } />
      }
    })
  }

  onCheck = (checkedKeys, info) => {
    console.log('onCheck', checkedKeys, info);
    this.props.patchMenuInfo(checkedKeys)
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const detail_info = this.props.detailInfo;

    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 }
    }

    return (
      <Form layout="horizontal" { ...formItemLayout }>
        <FormItem label="角色名称">
          {
            getFieldDecorator('role_name')(
              <Input style={ { width: 120 } } type="text" placeholder={ detail_info.role_name } disabled />
            )
          }
        </FormItem>
        <FormItem label="状态">
          {
            getFieldDecorator('status', {
              initialValue: 1
            })(
              <Select style={ { width: 120 } }>
                <Option value={ 1 }>启用</Option>
                <Option value={ 0 }>禁用</Option>
              </Select>
            )
          }
        </FormItem>
        <Tree
          checkable
          defaultExpandAll
          onCheck={ this.onCheck }
          checkedKeys={ this.props.menuInfo }
        >
          <TreeNode title="平台权限" key="platform_all">
            { this.renderThreeNodes(menuConfig) }
          </TreeNode>
        </Tree>
      </Form>
    )
  }
}

export default PermForm = Form.create({})(PermForm);