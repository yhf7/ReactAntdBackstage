import React, { Component } from 'react'
import { Card } from 'antd'
// 引入主题色
import echartTheme from './../echartTheme'
// 组件化 图表语法插件
import ReactEcharts from "echarts-for-react";
// 按需加载
import echarts from 'echarts/lib/echarts'
// 导入柱形图
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/markPoint'

export default class Bar extends Component {

  componentWillMount () {
    // 加载注入
    echarts.registerTheme('Imooc', echartTheme);
  }

  getOption = () => {
    let option = {
      title: {
        text: '用户骑行订单'
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: [
        {
          type: 'category',
          data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
          axisTick: {
            alignWithLabel: true
          }
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: '订单量',
          type: 'bar',
          data: [1000, 2900, 2050, 3634, 190, 330, 220]
        }
      ]
    };
    return option
  }

  getOption2 = () => {
    let option = {
      title: {
        text: '用户骑行订单'
      },
      legend: {
        data: ['oFo', '膜拜', '小蓝']
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: [
        {
          type: 'category',
          data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
          axisTick: {
            alignWithLabel: true
          }
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'oFo',
          type: 'bar',
          data: [6000, 2900, 2050, 3634, 190, 330, 220]
        },
        {
          name: '膜拜',
          type: 'bar',
          data: [100, 2990, 1050, 3034, 5550, 330, 660]
        },
        {
          name: '小蓝',
          type: 'bar',
          data: [100, 2920, 2065, 3634, 1900, 3308, 2220]
        }
      ]
    };
    return option
  }

  render() {
    return (
      <div>
        <Card title="柱形图表之一">
          <ReactEcharts option={this.getOption()} theme="Imooc" style={{height: 500}} />
        </Card>
        <Card title="柱形图表之二" style={{marginTop: 20}}>
          <ReactEcharts option={ this.getOption2() } theme="Imooc" style={ { height: 500 } } />
        </Card>
      </div>
    )
  }
}