import React,{Component} from 'react'
import { Card,Spin,Icon, Alert } from 'antd';
import '../ui.less'

export default class extends Component {
  render () {
    const icon = <Icon type="loading"/>
    return (
      <div>
        <Card title="Spin用法">
          <Spin size="small" />
          <Spin />
          <Spin size="large" />
          <Spin tip="Loading..." indicator={icon}/>
        </Card>
        <Card className="card-top">
          <Alert
            message="React"
            description="欢迎来到YHF高级实战开发"
            type="info"
          />
          <Spin>
            <Alert
              message="React"
              description="欢迎来到YHF高级实战开发"
              type="warning"
            />
          </Spin>
          <Spin tip="加载中...">
            <Alert
              message="React"
              description="欢迎来到YHF高级实战开发"
              type="warning"
            />
          </Spin>
          <Spin indicator={ icon }>
            <Alert
              message="React"
              description="欢迎来到YHF高级实战开发"
              type="warning"
            />
          </Spin>
        </Card>
      </div>
    )
  }
}