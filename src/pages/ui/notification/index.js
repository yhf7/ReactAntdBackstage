import React,{Component} from 'react'
import '../ui.less'
import { Card, Button, notification } from 'antd'

export default class Notification extends Component {
  render () {
    const openNotificationWithIcon = (type,plac = 'topRight') => {
      notification[type]({
        message: '发工资',
        placement:plac,
        description:
          '上个月考勤22天，迟到12天，实发工资250，请笑纳',
      });
    };

    return (
      <div>
        <Card title="通知提醒框" className="card-wrap">
          <Button type="primary" onClick={ () => openNotificationWithIcon('success') }>Success</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon('info') }>Info</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon('warning') }>Warning</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon('error') }>Error</Button>
        </Card>
        <Card title="控制输出位置通知提醒框" className="card-wrap card-top">
          <Button type="primary" onClick={ () => openNotificationWithIcon('success', 'topLeft') }>Success</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon('info', 'topRight') }>Info</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon('warning', 'bottomLeft') }>Warning</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon('error', 'bottomRight') }>Error</Button>
        </Card>
      </div>
    )
  }
}