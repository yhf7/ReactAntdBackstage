import React, { Component } from 'react';
import { Card, Button, Icon, Radio} from 'antd'
import '../ui.less'

export default class Buttons extends Component {

  state = {
    loading: true,
    name: '关闭',
    size: 'default'
  }

  /**
   * loading控制开关按钮函数
   */
  handleCloseLoading = () => {
    if (this.state.name === '关闭') {
      this.setState({
        loading: false,
        name: '开启'
      })
    } else {
      this.setState({
        loading: true,
        name: '关闭'
      })
    }
  }

  /**
   * 单选按钮，处理单击按钮改变大小的方法
   */
  onChange = e => {
    this.setState({
      size: e.target.value
    });
  };

  render() {
    return (
      <div>
        <Card title="基础按钮" className="card-wrap">
          <Button type="primary">主按钮</Button>
          <Button>次按钮</Button>
          <Button type="dashed">虚线按钮</Button>
          <Button type="danger">危险按钮</Button>
          <Button disabled>禁用按钮</Button>
          <Button type="link">链接按钮</Button>
        </Card>
        <Card title="图形按钮" className="card-wrap card-top">
          <Button icon="plus">创建</Button>
          <Button icon="edit">编辑</Button>
          <Button icon="delete">删除</Button>
          <Button icon="search" shape="circle"></Button>
          <Button type="primary" icon="search">搜索</Button>
          <Button type="primary" icon="download">下载</Button>
        </Card>
        <Card title="Loading按钮" className="card-wrap card-top">
          <Button type="primary" loading={this.state.loading}>确定</Button>
          <Button type="primary" shape="circle" loading={ this.state.loading }></Button>
          <Button loading={ this.state.loading }>点击加载</Button>
          <Button shape="circle" loading={ this.state.loading }></Button>
          <Button type="primary" onClick={ this.handleCloseLoading }>{ this.state.name }</Button>
        </Card>
        <Card title="按钮组" className="card-top">
          <Button.Group>
            <Button type="primary">
              <Icon type="left" />
                左
            </Button>
            <Button type="primary">
                右
              <Icon type="right" />
            </Button>
          </Button.Group>
        </Card>
        <Card title="按钮尺寸" className="card-wrap card-top">
          <Radio.Group onChange={ this.onChange } value={ this.state.size }>
            <Radio value={ "small" }>小</Radio>
            <Radio value={ "default" }>中</Radio>
            <Radio value={ "large" }>大</Radio>
          </Radio.Group>
          <Button type="primary" size={this.state.size}>
            Primary
          </Button>
          <Button size={this.state.size}>Default</Button>
          <Button type="dashed" size={this.state.size}>
            Dashed
          </Button>
          <Button type="danger" size={this.state.size}>
            danger
          </Button>
        </Card>
      </div>
    );
  }
}