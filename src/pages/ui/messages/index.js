import React, { Component } from 'react'
import '../ui.less'
import { Card, Button, message } from 'antd'

export default class Messages extends Component {
  render() {
    const openMessages = (type) => {
      message[type]('恭喜你，React课程晋级成功');
    };

    return (
      <div>
        <Card title="全局提醒框" className="card-wrap">
          <Button type="primary" onClick={ () => openMessages('success') }>Success</Button>
          <Button type="primary" onClick={ () => openMessages('info') }>Info</Button>
          <Button type="primary" onClick={ () => openMessages('warning') }>Warning</Button>
          <Button type="primary" onClick={ () => openMessages('error') }>Error</Button>
          <Button type="primary" onClick={ () => openMessages('loading') }>Loading</Button>
        </Card>
      </div>
    )
  }
}