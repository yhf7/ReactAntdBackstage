import React,{Component} from 'react'
// antd 栅格布局
import { Row, Col } from 'antd';
// 左侧边栏
import NavLeft from './components/NavLeft'
// 上下头部
import Header from './components/Header';
import Footer from './components/Footer'
// 全局样式
import './style/common.less'

export default class Admin extends Component {
  render () {
    return (
      <Row className="container">
        <Col span={ 3 } className="nav-left">
            <NavLeft/>
        </Col>
        <Col span={ 21 } className="main">
          <Header/>
          <Row className="content">
            { this.props.children }
          </Row>
          <Footer/>
        </Col>
      </Row>
    )
  }
}