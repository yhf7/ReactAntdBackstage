/**
 * 引入createStore
 */

import { createStore } from 'redux'
import reducer from './../reducer'
// import { composeWithDevTools } from 'redux-devtools-extension' // 调试插件
const initialState = {
  menuName: '首页'
}
export default () => createStore(reducer, initialState)