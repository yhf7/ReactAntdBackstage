//  事件促发行为

/**
 * Action 类型
 */
export const type = {
  SWITCH_MENU: 'SWITCH_MENU'
}

/**
 * 切换菜单类型
 * @param {*} menuName 菜单名称
 */
export function switchMenu(menuName) {
  return {
    type: type.SWITCH_MENU,
    menuName
  }
}